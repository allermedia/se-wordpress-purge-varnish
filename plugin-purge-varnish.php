<?php
/*
Plugin Name: Purge Varnish
Description: This plugin provides integration between your wordpress site and Varnish Cache to purge cache objects.
Author: Joel Bernerman, DP Singh <dev.firoza@gmail.com>
Version: 2.3.8
License: GPL2+
*/


$GLOBALS['purgeVarnishPath'] = plugin_dir_path(__FILE__);
$GLOBALS['purgeVarnishUrl'] = plugins_url('', __FILE__);
$GLOBALS['purgeVarnishVersion'] = '2.3.8';
$GLOBALS['purgeVarnishPluginFile'] = __FILE__;
$GLOBALS['purgeVarnishDebug'] = true;

// Autoloader
require __DIR__ . '/vendor/autoload.php';

// Register our container for dependency injection
$builder = new DI\ContainerBuilder();
$container = $builder->build();

// Run the plugin itself
$plugin = $container->get('PurgeVarnish\Plugin');
$plugin->init();
