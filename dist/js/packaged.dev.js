'use strict';

jQuery(document).ready(function () {
  if (jQuery('#post_expiration').length) {
    jQuery('#post_expiration').css('display', 'block');
  }

  jQuery('.ck_custom_url').change(function (e) {
    if (e.currentTarget.checked) {
      jQuery('div.custom_url').removeClass('hide_custom_url');
      jQuery('div.custom_url').addClass('show_custom_url');
    } else {
      jQuery('div.custom_url').removeClass('show_custom_url');
      jQuery('div.custom_url').addClass('hide_custom_url');
    }
  });
});

/**
 * Switch between tabs.
 *
 * @param {object} event
 * @param {integer} containerId
 */
var openTab = function openTab(event, containerId) {
  // Declare all variables
  var i = void 0;
  var target = event.currentTarget;

  // Get all elements with class="tabcontent" and hide them
  var tabContent = document.getElementsByClassName('tabcontent');
  for (i = 0; i < tabContent.length; i += 1) {
    tabContent[i].style.display = 'none';
  }

  // Get all elements with class="tablinks" and remove the class "active"
  var tabLinks = document.getElementsByClassName('tablinks');
  for (i = 0; i < tabLinks.length; i += 1) {
    tabLinks[i].className = tabLinks[i].className.replace(' active', '');
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(containerId).style.display = 'block';
  target.className += ' active';
};