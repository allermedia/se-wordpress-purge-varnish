// Include gulp
const gulp = require('gulp');

// Include Our Plugins
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cssnano = require('gulp-cssnano');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const imagemin = require('gulp-imagemin');
const bump = require('gulp-bump');
const argv = require('yargs').default('release', 'patch').argv; // eslint-disable-line prefer-destructuring
const fs = require('file-system');
const replace = require('gulp-replace');
const semver = require('semver');
const git = require('gulp-git');
const babel = require('gulp-babel');

// Parses the package.json file. We use this because its values
// change during execution.
const getPackageJSON = () => {
  const result = JSON.parse(fs.readFileSync('./package.json', 'utf8'));
  return result;
};

// Compile Our Sass
gulp.task('sass-dist', () => {
  const result = gulp.src('source/scss/style.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(cssnano({
      mergeLonghand: false,
      zindex: false,
    }))
    .pipe(gulp.dest('dist/css'));
  return result;
});

gulp.task('sass-dev', () => {
  const result = gulp.src('source/scss/style.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
    .pipe(rename({ suffix: '.dev' }))
    .pipe(gulp.dest('dist/css'));
  return result;
});

// Compress images
gulp.task('compress-images', () => {
  const result = gulp.src('source/image/**')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/image'));
  return result;
});

// Concatenate & Minify JS
gulp.task('scripts-dist', () => {
  const result = gulp.src(['source/js/**/*.js', 'source/js/main.js'])
    .pipe(plumber({
      errorHandler: (err) => {
        console.log(err); // eslint-disable-line no-console
        this.emit('end');
      },
    }))
    .pipe(babel({
      presets: ['env'],
    }))
    .pipe(concat('packaged.dev.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(rename('packaged.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
  return result;
});

// Bump versions
gulp.task('bump', () => {
  // Get package.json
  const pkg = getPackageJSON();
  const newversion = semver.inc(pkg.version, argv.release);

  // Wordpress banner
  const banner = ['/*',
    `Plugin Name: ${pkg.title}`,
    `Description: ${pkg.description}`,
    `Author: ${pkg.author}`,
    `Version: ${newversion}`,
    'License: GPL2+',
    '*/'].join('\n');

  const constant = [`$GLOBALS['purgeVarnishVersion'] = '${newversion}';`];

  // Bump package.json
  gulp.src('./package.json')
    .pipe(bump({ version: newversion }))
    .pipe(gulp.dest('./'));
  gulp.src('./package-lock.json')
    .pipe(bump({ version: newversion }))
    .pipe(gulp.dest('./'));

  // Bump wordpress plugin file
  const regex = /\/\*\nPlugin(.|[\r\n])*?\*\//;
  const constantRegex = /\$GLOBALS\['purgeVarnishVersion'\] = '.*?';/;

  gulp.src('./*.php')
    .pipe(replace(regex, banner))
    .pipe(replace(constantRegex, constant))
    .pipe(gulp.dest('./'));

  // Create git tag
  git.tag(`v${newversion}`, `${pkg.description} v${newversion}`, { quiet: false }, () => {});
  return true;
});

// Watch Files For Changes
gulp.task('watch', () => {
  gulp.watch(['source/js/*.js', 'source/js/**/*.js'], ['scripts-dist']);
  gulp.watch(['source/scss/**/*.scss'], ['sass-dist', 'sass-dev']);
});

gulp.task('build', ['sass-dist', 'sass-dev', 'scripts-dist', 'compress-images']);

// Default Task
gulp.task('default', ['build']);
