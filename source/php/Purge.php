<?php

namespace PurgeVarnish;

/**
 * Purge cache class.
 */
class Purge
{

    /**
     * Constructor.
     *
     * @param Helper   $Helper
     * @param Store    $Store
     * @param Terminal $Terminal
     */
    public function __construct(Helper $Helper, Store $Store, Terminal $Terminal)
    {
        $this->Helper = $Helper;
        $this->Store = $Store;
        $this->Terminal = $Terminal;
    }

    /**
     * Purges custom urls.
     *
     * @param string $urlType
     * @param string $urlTypePlural
     * @param array  $expire
     * @return void
     */
    public function customUrls($urlType, $urlTypePlural, $expire)
    {
        $expireCustomUrl = isset($expire[$urlType]) ? $expire[$urlType] : '';
        if (!empty($expireCustomUrl)) {
            $customUrls = isset($expire[$urlTypePlural]) ? $expire[$urlTypePlural] : '';
            $customUrls = explode("\r\n", $customUrls);
            if (count($customUrls)) {
                foreach ($customUrls as $url) {
                    $command = $this->getCommand($url);
                    $this->Terminal->run($command);
                }
            }
        }
    }

    /**
     * Purges front page object.
     *
     * @return void
     */
    public function frontPage()
    {
        $url = get_home_url();
        $command = $this->getCommand($url, 'front');
        $this->Terminal->run($command);
    }

    /**
     * Purges post object.
     *
     * @param WP_Post $post
     * @return void
     */
    public function postItem($post)
    {
        $url = get_permalink($post->ID);
        $command = $this->getCommand($url);
        $this->Terminal->run($command);
    }

    /**
     * Purges post releated category and tag objects.
     *
     * @param WP_Post $post
     * @return void
     */
    public function categoryAndTagPage($post)
    {
        $cats = wp_get_post_categories($post->ID);
        foreach ($cats as $categoryId) {
            $url = get_category_link($categoryId);
            $command = $this->getCommand($url);
            $this->Terminal->run($command, 'category');
        }

        $tags = wp_get_post_tags($post->ID);
        foreach ($tags as $tag) {
            $url = get_tag_link($tag->term_id);
            $command = $this->getCommand($url, 'category');
            $this->Terminal->run($command);
        }
    }

    /**
     * Purges entire cache.
     *
     * @return void
     */
    public function allCache()
    {
        $url = get_home_url();
        $command = $this->getCommand($url, 'purgeall');
        $this->Terminal->run($command);
    }

    /**
     * Callback to purge nav menu items.
     *
     * @param array $items
     * @return void
     */
    public function menuNavLinks($items)
    {
        foreach ($items as $item) {
            $url = $item->url;
            $command = $this->getCommand($url);
            $this->Terminal->run($command);
        }
    }


    /**
     * Purge all multisite frontpages.
     *
     * @return void
     */
    public function multisiteFrontPage()
    {
        $sites = get_sites();

        $scheme = 'http://';
        if (is_ssl()) {
            $scheme = 'https://';
        }

        $url = '';
        $command = [];
        foreach ($sites as $site) {
            $url = $scheme . $site->domain . $site->path;
            $command = array_merge($command, $this->getCommand($url, 'front'));
        }

        $this->Terminal->run($command);
    }

    /**
     * Purge all multisite categories and tags.
     *
     * @return void
     */
    public function multisiteCategoryAndTagPage()
    {
        $sites = get_sites();

        $scheme = 'http://';
        if (is_ssl()) {
            $scheme = 'https://';
        }

        $url = '';
        $command = [];
        foreach ($sites as $site) {
            $categoryBase = get_blog_option($site->blog_id, 'category_base', 'category');
            $tagBase = get_blog_option($site->blog_id, 'tag_base', 'tag');
            $categoryBase = empty($categoryBase) ? 'category' : $categoryBase;
            $tagBase = empty($tagBase) ? 'tag' : $tagBase;

            $url = $scheme . $site->domain . $site->path . $categoryBase;
            $command = array_merge($command, $this->getCommand($url, 'category'));

            $url = $scheme . $site->domain . $site->path . $tagBase;
            $command = array_merge($command, $this->getCommand($url, 'category'));
        }

        $this->Terminal->run($command);
    }


    /**
     * Build command to purge the cache.
     *
     * @param string $url
     * @param string $flag
     * @return string
     */
    public function getCommand($url, $flag = '')
    {
        $parseUrl = $this->Helper->ParseUrl($url);
        $host = isset($parseUrl['host']) ? $parseUrl['host'] : '';
        $path = isset($parseUrl['path']) ? $parseUrl['path'] : '';
        $command = [];

        if ($flag == 'front') {
            $command[] = 'ban req.http.host == "' . $host . '" && req.url ~ "^' . $path . '$"';
            $command[] = 'ban req.http.host == "' . $host . '" && req.url ~ "^' . $path . 'page/.*$"';
        } elseif ($flag == 'category') {
            $command[] = 'ban req.http.host == "' . $host . '" && req.url ~ "^' . $path . '/.*$"';
            $command[] = 'ban req.http.host == "' . $host . '" && req.url ~ "^' . $path . '/page/.*$"';
        } elseif ($flag == 'purgeall') {
            $command[] = 'ban req.http.host == "' . $host . '"';
        } else {
            $command[] = 'ban req.http.host == "' . $host . '" && req.url ~ "^' . $path . '$"';
        }

        return $command;
    }

    /**
     * Helper function to clear all varnish cache.
     *
     * @return array
     */
    public function allCacheManually()
    {
        $url = get_home_url();
        $parseUrl = $this->Helper->parseUrl($url);
        $host = $parseUrl['host'];

        $command = "ban req.http.host == \"$host\"";
        $response = $this->Terminal->run($command);

        $message = '';
        foreach ($this->Terminal->getTerminals() as $terminal) {
            $responseStatus = $response[$terminal][$command];

            if ($responseStatus['code'] == 200) {
                $message .= '<li style="color:#228B22;list-style-type: circle;">
                    All Varnish cache has been purged successfully!</li>';
            } else {
                $message .= '<li style="color:#8B0000;list-style-type: circle;">
                    There was an error, Please try later!</li>';
            }
        }

        return $message;
    }

    /**
     * Helper function to parse the host from the global $base_url.
     *
     * @param array $urls
     * @return string
     */
    public function urls($urls)
    {
        $outout = '';
        $purgeMessage = '';
        if (!count($urls)) {
            return '<ul><li style="color:#8B0000;">' .
                esc_html_e('Please enter at least one url for purge.') . '</li></ul>';
        }

        foreach ($urls as $url) {
            $url = trim(esc_url($url));
            $command = $this->getCommand($url);
            $response = $this->Terminal->run($command);
            $purgeMessage = $this->urlMsg($url, $command, $response, $msg);
        }

        $outout .= '<ul>';
        $outout .= $purgeMessage;
        $outout .= '</ul>';

        return $outout;
    }

    /**
     * Purge everything if user triggers it.
     *
     * @return string
     */
    public function purgeAllOnSave()
    {
        $message = '';
        if (isset($_POST['purge_all']) && $_POST['purge_all'] == 'Purge all') {
            if ($this->Store->nonce('purgeAllCache') == true) {
                $message = $this->allCacheManually();
            }
        }
        return $message;
    }

    /**
     * Purge urls if user triggers it.
     *
     * @return string
     */
    public function purgeUrlsOnSave()
    {
        $message = '';
        if (isset($_POST['purge']) && $_POST['purge'] == 'Purge') {
            if ($this->Store->nonce('pvUrls') == true) {
                $urls = array_filter($_POST['urls']);
                // Sanitize internally.
                $message = $this->urls($urls);
            }
        }
        return $message;
    }

    /**
     * Message to display after purging cache.
     *
     * @param array  $urls
     * @param string $command
     * @param array  $response
     * @param string $message
     * @return string
     */
    public function urlMsg($urls, $command, $response, &$message)
    {
        $parseUrl = $this->parseUrl($urls);

        foreach ($this->Terminal->getTerminals() as $terminal) {
            $respStatus = $response[$terminal][$command];
            $statusCode = $respStatus['code'];
            $statusMsg = $respStatus['msg'];

            if ($statusCode == 200) {
                if (esc_url($parseUrl['path']) == '/') {
                    $statusMessage = 'The <a href="' . esc_url($_POST['url']) .
                        '" target="@target" style="color:#228B22;">
                        <i>"front/home page url"</i></a> has been purged.';

                    $message .= '<li style="color:#228B22;list-style-type: circle;">' . $statusMessage . '</li>';
                } else {
                    $statusMessage = 'The page url <a href="' . esc_url($_POST['url']) .
                        '" target="@target" style="color:#228B22;"><i>"' . esc_url($parseUrl['path']) .
                        '"</i></a> has been purged.';

                    $message .= '<li style="color:#228B22;list-style-type: circle;">' . $statusMessage . '</li>';
                }
            } else {
                $statusMessage = 'The url <a href="' . esc_url($_POST['url']) .
                    '" target="@target" style="color:#8B0000;"><i>"' . esc_url($parseUrl['path']) .
                    '"</i></a> has not been purged.';

                $message .= '<li style="color:#8B0000;list-style-type: circle;">' . $statusMessage . '</li>';
            }
        }

        return $message;
    }
}
