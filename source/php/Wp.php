<?php

namespace PurgeVarnish;

/**
 * Wordpress related functions.
 */
class Wp
{
    public $menuKey = 'purge-varnish-settings';

    /**
     * Constructor.
     *
     * @param Helper           $Helper
     * @param \Mustache_Engine $Mustache
     * @param Purge            $Purge
     * @param Store            $Store
     * @param Template         $Template
     */
    public function __construct(
        Helper $Helper,
        \Mustache_Engine $Mustache,
        Purge $Purge,
        Store $Store,
        Template $Template
    ) {
        $this->Helper = $Helper;
        $this->Mustache = $Mustache;
        $this->Purge = $Purge;
        $this->Store = $Store;
        $this->Template = $Template;
    }

    /**
     * Implements hook for register styles and scripts.
     *
     * @return void
     */
    public function enqueue()
    {
        $suffix = 'min';
        if ($GLOBALS['purgeVarnishDebug']) {
            $suffix = 'dev';
        }
        wp_enqueue_style(
            'purge-varnish',
            $GLOBALS['purgeVarnishUrl'] . '/dist/css/style.' . $suffix . '.css',
            [],
            $GLOBALS['purgeVarnishVersion']
        );

        wp_enqueue_script(
            'purge-varnish',
            $GLOBALS['purgeVarnishUrl'] . '/dist/js/packaged.' . $suffix . '.js',
            [],
            $GLOBALS['purgeVarnishVersion']
        );
    }

    /**
     * Add menus.
     *
     * @return void
     */
    public function addMenus()
    {

        $menuFunction = [
            $this,
            'pageFilePath'
        ];

        add_menu_page(
            'Purge Varnish',
            'Purge Varnish',
            'manage_options',
            $this->menuKey,
            $menuFunction,
            $GLOBALS['purgeVarnishUrl'] . '/dist/image/purge16x16.png',
            $GLOBALS['purgeVarnishVersion']
        );

        if (current_user_can('manage_options')) {
            add_submenu_page(
                $this->menuKey,
                'Terminal',
                'Terminal',
                'manage_options',
                $this->menuKey,
                $menuFunction
            );
            add_submenu_page(
                $this->menuKey,
                'Purge all',
                'Purge all',
                'manage_options',
                'purge-varnish-all',
                $menuFunction
            );

            add_submenu_page(
                $this->menuKey,
                'Expire',
                'Expire',
                'manage_options',
                'purge-varnish-expire',
                $menuFunction
            );
            if (is_multisite()) {
                add_submenu_page(
                    $this->menuKey,
                    'Multisite',
                    'Multisite',
                    'edit_posts',
                    'purge-varnish-multisite',
                    $menuFunction
                );
            }
        }

        if (current_user_can('edit_posts')) {
            add_submenu_page(
                $this->menuKey,
                'Purge URLs',
                'Purge URLs',
                'edit_posts',
                'purge-varnish-urls',
                $menuFunction
            );
        }
    }

    /**
     * Fetch template and data decided on what page you are on.
     *
     * @return void
     */
    public function pageFilePath()
    {
        $screen = get_current_screen();

        if (strpos($screen->base, 'purge-varnish-settings') !== false) {
            $this->Store->storeTerminal();
            $data = $this->Template->getTerminalTemplateData();
            $template = $this->Template->getTemplate('terminal.html');
            echo $this->Mustache->render($template, $data);
        } elseif (strpos($screen->base, 'purge-varnish-all') !== false) {
            $message = $this->Purge->purgeAllOnSave();
            $data = $this->Template->getPurgeAllTemplateData();
            $template = $this->Template->getTemplate('purge-all.html');
            $data['message'] = $message;
            echo $this->Mustache->render($template, $data);
        } elseif (strpos($screen->base, 'purge-varnish-expire') !== false) {
            $this->Store->storeExpire($this->defaultActions(), $this->defaultTriggers());
            $data = $this->Template->getExpireTemplateData();
            $template = $this->Template->getTemplate('expire.html');
            echo $this->Mustache->render($template, $data);
        } elseif (strpos($screen->base, 'purge-varnish-urls') !== false) {
            $message = $this->Purge->purgeUrlsOnSave();
            $data = $this->Template->getPurgeUrlsTemplateData();
            $template = $this->Template->getTemplate('purge-urls.html');
            $data['message'] = $message;
            echo $this->Mustache->render($template, $data);
        } elseif (strpos($screen->base, 'purge-varnish-multisite') !== false) {
            $message = $this->Store->storeMultisite();
            $data = $this->Template->getMultisiteTemplateData();
            $template = $this->Template->getTemplate('purge-multisite.html');
            $data['message'] = $message;
            echo $this->Mustache->render($template, $data);
        }
    }

    /**
     * Implements hook for add settings link.
     *
     * @param array $links
     * @return array
     */
    public function settingsLink($links)
    {
        $settings_link = '<a href="' . esc_url(admin_url('/admin.php?page='. $this->menuKey)) . '">'
            . __('Settings', 'Purge Varnish') . '</a>';
        array_unshift($links, $settings_link);
        return $links;
    }

    /**
     * Actions perform on activation of plugin.
     *
     * @return void
     */
    public function install()
    {
        $actions = $this->defaultActions();
        $this->Helper->updateOption('purge_varnish_action', serialize($actions));

        $triggers = $this->defaultTriggers();
        $this->Helper->updateOption('purge_varnish_expire', serialize($triggers));
    }

    /**
     * Actions perform on de-activation of plugin.
     *
     * @return void
     */
    public function uninstall()
    {
        $this->Helper->deleteOption('varnish_version');
        $this->Helper->deleteOption('varnish_control_terminal');
        $this->Helper->deleteOption('varnish_control_key');
        $this->Helper->deleteOption('varnish_socket_timeout');
        $this->Helper->deleteOption('purge_varnish_action');
        $this->Helper->deleteOption('purge_varnish_expire');
        $this->Helper->deleteOption('purge_varnish_multisite_action');
        $this->Helper->deleteOption('purge_varnish_multisite_expire');
        $this->Helper->deleteOption('purge_varnish_sitewide_options');
    }

    /**
     * Key=>Value pair of default actions.
     *
     * @return void
     */
    public function defaultActions()
    {
        return [
          'post_insert_update' => 'post_updated',
          'post_status' => 'transition_post_status',
          'post_trash' => 'wp_trash_post',
          'comment_insert_update' => 'comment_post',
          'navmenu_insert_update' => 'wp_update_nav_menu',
          'theme_switch' => 'after_switch_theme',
        ];
    }

    /**
     * Key=>Value pair of default triggers.
     *
     * @return void
     */
    public function defaultTriggers()
    {
        return [
          'post_front_page' => 'front_page',
          'post_custom_url' => 'custom_url',
          'post_post_item' => 'post_item',
          'post_category_page' => 'category_page',
          'comment_front_page' => 'front_page',
          'comment_custom_url' => 'custom_url',
          'navmenu_front_page' => 'front_page',
          'navmenu_custom_url' => 'custom_url',
          'wp_theme_front_page' => 'front_page',
          'wp_theme_custom_url' => 'custom_url',
        ];
    }
}
