<?php

namespace PurgeVarnish;

/**
 * Trigger stuff.
 */
class Trigger
{

    /**
     * Constructor.
     *
     * @param Helper $Helper
     * @param Purge  $Purge
     */
    public function __construct(Helper $Helper, Purge $Purge)
    {
        $this->Helper = $Helper;
        $this->Purge = $Purge;

        $expire = $this->Helper->getOption('purge_varnish_expire', '');
        if (!empty($expire)) {
            $expire = unserialize($expire);
        }
        $this->expire = $expire;

        $multisiteExpire = $this->Helper->getOption('purge_varnish_multisite_expire', '');
        if (!empty($multisiteExpire)) {
            $multisiteExpire = $multisiteExpire;
        }
        $this->multisiteExpire = $multisiteExpire;
    }

    /**
     * Callback to purge various type of varnish objects on publish post inserted/updated.
     *
     * @param integer $id
     * @return void
     */
    public function postUpdatedTrigger($id)
    {
        $post = get_post($id);

        if ($this->validatePostUpdatedTrigger($post)) {
            // Callback to purge.
            $this->triggerPostExpire($post);
        }
    }

    /**
     * Callback to handle any multisite purges.
     */
    public function postUpdatedMultisiteTrigger($id)
    {
        $post = get_post($id);

        if ($this->validatePostUpdatedTrigger($post)) {
            // Callback to purge.
            $this->triggerMultisitePurge($post);
        }
    }

    /**
     * Validate that the post update is correct for trigereing purge.
     *
     * @param WP_Post $post The post object
     * @return boolean
     */
    private function validatePostUpdatedTrigger($post)
    {
        // Halt exection if not have post object.
        if (!$this->Helper->isPostObject($post)) {
            return false;
        }

        // Halt exection if post type is nav_menu_item.
        if ($this->Helper->postIsNavMenuItem($post)) {
            return false;
        }

        // Halt exection if post status in not publish.
        if (!$this->Helper->isPublishPostObject($post)) {
            return false;
        }

        // Halt exection if post type is attachment.
        if ($this->Helper->postIsAttachment($post)) {
            return false;
        }

        return true;
    }

    /**
     * Callback to purge various type of varnish objects on post changed.
     *
     * @param string  $newStatus
     * @param string  $oldStatus
     * @param WP_Post $post
     * @return void
     */
    public function postStatusTrigger($newStatus, $oldStatus, $post)
    {
        if ($this->validatePostStatusTrigger($newStatus, $oldStatus, $post)) {
            $this->triggerPostExpire($post);
        }
    }

    /**
     * Callback to purge multisite on post status change.
     *
     * @param string  $newStatus
     * @param string  $oldStatus
     * @param WP_Post $post
     * @return void
     */
    public function postStatusMultisiteTrigger($newStatus, $oldStatus, $post)
    {
        if ($this->validatePostStatusTrigger($newStatus, $oldStatus, $post)) {
            // Callback to purge.
            $this->triggerMultisitePurge($post);
        }
    }

    /**
     * Validate that status is correct for purge.
     *
     * @param string  $newStatus
     * @param string  $oldStatus
     * @param WP_Post $post
     */
    private function validatePostStatusTrigger($newStatus, $oldStatus, $post)
    {
        // Halt exection if not have post object
        if (!$this->Helper->isPostObject($post)) {
            return false;
        }

        // Halt exection if post type is attachment.
        if ($this->Helper->postIsAttachment($post)) {
            return false;
        }

        // Cause with wp_update_nav_menu
        if ($newStatus == $oldStatus) {
            return false;
        }

        // Cause with wp_update_nav_menu
        if ($newStatus == 'auto-draft' && $oldStatus == 'new') {
            return false;
        }

        if ($newStatus == 'publish' || $oldStatus == 'publish') {
            return true;
        }
        return false;
    }

    /**
     * Callback to purge various type of varnish objects on post trash action.
     *
     * @param integer $id
     * @return void
     */
    public function wpTrashPostTrigger($id)
    {
        // Fetch post object.
        $post = get_post($id);

        if ($this->validateWpTrashPostTrigger($post)) {
            // Call to purge
            $this->triggerPostExpire($post);
        }
    }

    /**
     * Callback to purge multisite varnish objects on post trash action.
     *
     * @param integer $id
     * @return void
     */
    public function wpTrashPostMultisiteTrigger($id)
    {
        // Fetch post object.
        $post = get_post($id);

        if ($this->validateWpTrashPostTrigger($post)) {
            $this->triggerMultisitePurge($post);
        }
    }

    /**
     * Validate the trash post is good to trigger purge-
     *
     * @param WP_Post $post
     * @return boolean
     */
    private function validateWpTrashPostTrigger($post)
    {
        // Halt exection if not have post object
        if (!$this->Helper->isPostObject($post)) {
            return false;
        }
        // Halt exection if post status in not publish.
        if (!$this->Helper->isPublishPostObject($post)) {
            return false;
        }
        // Halt exection if post type is attachment.
        if ($this->Helper->postIsAttachment($post)) {
            return false;
        }

        return true;
    }

    /**
     * Callback to purge various type of varnish objects on comment edited.
     *
     * @param integer $id
     * @return void
     */
    public function commentPostTrigger($id)
    {
        $comment = get_comment($id);

        if (!is_object($comment)) {
            return;
        }

        $postId = $comment->comment_post_ID;
        $post = get_post($postId);

        if (is_object($comment) && ($comment->comment_approved <> 1 || $post->post_status <> 'publish')) {
            return;
        }
        // Call to purge
        $this->triggerCommentExpire($post);
    }

    /**
     * Callback to purge post object on comment status changed form.
     * approved => unapproved or unapproved => approved.
     *
     * @param string     $newStatus
     * @param string     $oldStatus
     * @param WP_Comment $comment
     * @return void
     */
    public function commentStatusTrigger($newStatus, $oldStatus, $comment)
    {
        // Cause with wp_update_nav_menu
        if ($newStatus == $oldStatus) {
            return;
        }

        if (($newStatus <> $oldStatus) && ($newStatus == 'approved' || $oldStatus == 'approved')) {
            $commentPostId = $comment->comment_post_ID;
            $post = get_post($commentPostId);

            // Halt exection if not have post object
            if (!$this->Helper->isPostObject($post)) {
                return;
            }
            // Halt exection if post type is attachment.
            if ($this->Helper->postIsAttachment($post)) {
                return;
            }

            $this->triggerPostExpire($post);
        } else {
            return;
        }
    }

    /**
     * Callback trigger to purge nav menu items.
     *
     * @param integer $navMenuId
     * @return void
     */
    public function updateNavMenuTrigger($navMenuId)
    {
        $items = wp_get_nav_menu_items($navMenuId);
        if (is_array($items) && count($items) > 0) {
            if (is_array($this->expire)) {
                foreach ($this->expire as $page) {
                    switch ($page) {
                        case 'front_page':
                            $this->Purge->frontPage();
                            break;

                        case 'post_item':
                            $this->Purge->menuNavLinks($items);
                            break;

                        case 'custom_url':
                            $this->Purge->customUrls('navmenu_custom_url', 'navmenu_custom_urls', $this->expire);
                            break;
                    }
                }
            }
        }
    }

    /**
     * Actions perform when switching theme.
     *
     * @return void
     */
    public function switchThemeTrigger()
    {
        if (is_array($this->expire)) {
            foreach ($this->expire as $page) {
                switch ($page) {
                    case 'front_page':
                        $this->Purge->frontPage();
                        break;

                    case 'purge_all':
                        $this->Purge->allCache();
                        break;

                    case 'custom_url':
                        $this->Purge->customUrls('wp_theme_custom_url', 'wp_theme_custom_urls', $this->expire);
                        break;
                }
            }
        }
    }

    /**
     * Prepare call for trigger post expire.
     *
     * @param WP_Post $post
     * @return void
     */
    private function triggerPostExpire($post)
    {
        if (is_array($this->expire)) {
            foreach ($this->expire as $page) {
                switch ($page) {
                    case 'front_page':
                        $this->Purge->frontPage();
                        break;

                    case 'post_item':
                        $this->Purge->postItem($post);
                        break;

                    case 'category_page':
                        $this->Purge->categoryAndTagPage($post);
                        break;

                    case 'custom_url':
                        $this->Purge->customUrls('post_custom_url', 'post_custom_urls', $this->expire);
                        break;
                }
            }
        }
    }

    /**
     * Prepare call for trigger comment expire.
     *
     * @param WP_Post $post
     * @return void
     */
    private function triggerCommentExpire($post)
    {
        if (is_array($this->expire)) {
            foreach ($this->expire as $page) {
                switch ($page) {
                    case 'front_page':
                        $this->Purge->frontPage();
                        break;

                    case 'post_item':
                        $this->Purge->postItem($post);
                        break;

                    case 'custom_url':
                        $this->Purge->customUrls('comment_custom_url', 'comment_custom_urls', $this->expire);
                        break;
                }
            }
        }
    }

    /**
     * Prepare call for trigger multisite expire.
     *
     * @param WP_Post $post
     * @return void
     */
    private function triggerMultisitePurge($post)
    {
        if (is_array($this->multisiteExpire)) {
            foreach ($this->multisiteExpire as $page) {
                switch ($page) {
                    case 'front_page':
                        $this->Purge->multisiteFrontPage();
                        break;

                    case 'category_tags_page':
                        $this->Purge->multisiteCategoryAndTagPage();
                        break;
                }
            }
        }
    }
}
