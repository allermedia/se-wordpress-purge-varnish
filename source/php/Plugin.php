<?php

namespace PurgeVarnish;

/**
 * Plugin initiator class.
 */
class Plugin
{
    /**
     * Constructor.
     *
     * @param Helper  $Helper
     * @param Trigger $Trigger
     * @param Wp      $Wp
     */
    public function __construct(Helper $Helper, Trigger $Trigger, Wp $Wp)
    {
        $this->Helper = $Helper;
        $this->Trigger = $Trigger;
        $this->Wp = $Wp;
    }

    /**
     * Initiate plugin.
     *
     * @return void
     */
    public function init()
    {
        add_action('admin_head', [$this->Wp, 'enqueue']);
        add_action('admin_menu', [$this->Wp, 'addMenus']);

        add_filter(
            'plugin_action_links_' . plugin_basename($GLOBALS['purgeVarnishPluginFile']),
            [
                $this->Wp,
                'settingsLink'
            ]
        );

        register_activation_hook(
            $GLOBALS['purgeVarnishPluginFile'],
            [$this->Wp, 'install']
        );

        register_deactivation_hook(
            $GLOBALS['purgeVarnishPluginFile'],
            [$this->Wp, 'uninstall']
        );

        // Trigger post action to purge varnish objects.
        $purgeVarnishAction = $this->Helper->getOption('purge_varnish_action', '');
        if (!empty($purgeVarnishAction)) {
            $actions = unserialize($purgeVarnishAction);
            if (is_array($actions)) {
                foreach ($actions as $action) {
                    switch ($action) {
                        case 'post_updated':
                            add_action($action, [$this->Trigger, 'postUpdatedTrigger']);
                            break;

                        case 'transition_post_status':
                            add_action($action, [$this->Trigger, 'postStatusTrigger'], 10, 3);
                            break;

                        case 'wp_trash_post':
                            add_action($action, [$this->Trigger, 'wpTrashPostTrigger']);
                            break;

                        case 'comment_post':
                            add_action($action, [$this->Trigger, 'commentPostTrigger']);
                            break;

                        case 'transition_comment_status':
                            add_action($action, [$this->Trigger, 'commentStatusTrigger'], 10, 3);
                            break;

                        case 'wp_update_nav_menu':
                            add_action($action, [$this->Trigger, 'updateNavMenuTrigger']);
                            break;

                        case 'after_switch_theme':
                            add_action($action, [$this->Trigger, 'switchThemeTrigger']);
                            break;
                    }
                }
            }
        }

        $purgeVarnishMutlisiteAction = $this->Helper->getOption('purge_varnish_multisite_action', '');
        if (!empty($purgeVarnishMutlisiteAction)) {
            $actions = $purgeVarnishMutlisiteAction;
            if (is_array($actions)) {
                foreach ($actions as $action) {
                    switch ($action) {
                        case 'post_updated':
                            add_action($action, [$this->Trigger, 'postUpdatedMultisiteTrigger']);
                            break;

                        case 'transition_post_status':
                            add_action($action, [$this->Trigger, 'postStatusMultisiteTrigger'], 10, 3);
                            break;

                        case 'wp_trash_post':
                            add_action($action, [$this->Trigger, 'wpTrashPostMultisiteTrigger']);
                            break;
                    }
                }
            }
        }
    }
}
