<?php

namespace PurgeVarnish;

/**
 * Handle the datafetching for templates.
 */
class Template
{
    /**
     * Constructor.
     *
     * @param Helper   $Helper
     * @param Terminal $Terminal
     */
    public function __construct(Helper $Helper, Terminal $Terminal)
    {
        $this->Helper = $Helper;
        $this->Terminal = $Terminal;
    }

    /**
     * Gather template data for purge url view.
     *
     * @return array
     */
    public function getPurgeUrlsTemplateData()
    {
        $data['urls'] = [];
        for ($i = 1; $i <= 7; $i++) {
            $data['urls'][] = [
                'text' => 'Enter ' . $this->Helper->numberSuffix($i) . ' URL :'
            ];
        }

        $data['nounceField'] = $this->getNonceField('purgeAllCache');
        $data['formAction'] = str_replace('%7E', '~', $_SERVER['REQUEST_URI']);
        return $data;
    }

    public function getMultisiteTemplateData()
    {
        $purgeVarnishAction = $this->Helper->getOption('purge_varnish_multisite_action', []);

        foreach ($purgeVarnishAction as $key => $action) {
            $data[$key] = $action;
        }

        $purgeVarnishExpire = $this->Helper->getOption('purge_varnish_multisite_expire', []);
        foreach ($purgeVarnishExpire as $key => $expire) {
            $data[$key] = $expire;
        }

        // Should always use get_site_option and not helper.
        $data['purge_varnish_sitewide_options'] = get_site_option('purge_varnish_sitewide_options', false);
        $data['nounceField'] = $this->getNonceField('purgeMultisite');
        $data['formAction'] = str_replace('%7E', '~', $_SERVER['REQUEST_URI']);
        return $data;
    }

    /**
     * Gather template data for expire view.
     *
     * @return array
     */
    public function getExpireTemplateData()
    {
        $data = [];

        // Get the post action options value
        $purgeVarnishAction = $this->Helper->getOption('purge_varnish_action', '');
        if (!empty($purgeVarnishAction)) {
            $action = unserialize($purgeVarnishAction);
            $data['postInsertUpdate'] = isset($action['post_insert_update'])
                ? $action['post_insert_update'] : '';
            $data['postStatus'] = isset($action['post_status'])
                ? $action['post_status'] : '';
            $data['postTrash'] = isset($action['post_trash'])
                ? $action['post_trash'] : '';
            $data['commentInsertUpdate'] = isset($action['comment_insert_update'])
                ? $action['comment_insert_update'] : '';
            $data['commentStatusChanged'] = isset($action['comment_status_changed'])
                ? $action['comment_status_changed'] : '';
            $data['navmenuInsertUpdate'] = isset($action['navmenu_insert_update'])
                ? $action['navmenu_insert_update'] : '';
            $data['afterSwitchTheme'] = isset($action['theme_switch'])
                ? $action['theme_switch'] : '';
        }

        // Get the post expire options value
        $purgeVarnishExpire = $this->Helper->getOption('purge_varnish_expire', '');

        if (!empty($purgeVarnishExpire)) {
            $expire = unserialize($purgeVarnishExpire);
            $data['expirePostFrontPage'] = isset($expire['post_front_page']) ?
                $expire['post_front_page'] : '';
            $data['expirePostPostItem'] = isset($expire['post_post_item']) ?
                $expire['post_post_item'] : '';
            $data['expirePostCategoryPage'] = isset($expire['post_category_page']) ?
                $expire['post_category_page'] : '';
            $data['expirePostCustomUrl'] = isset($expire['post_custom_url']) ?
                $expire['post_custom_url'] : '';
            $data['expirePostCustomUrls'] = isset($expire['post_custom_urls']) ?
                $expire['post_custom_urls'] : '';
            $data['expireCommentFrontPage'] = isset($expire['comment_front_page']) ?
                $expire['comment_front_page'] : '';
            $data['expireCommentPostItem'] = isset($expire['comment_post_item']) ?
                $expire['comment_post_item'] : '';
            $data['expireCommentCustomUrl'] = isset($expire['comment_custom_url']) ?
                $expire['comment_custom_url'] : '';
            $data['expireCommentCustomUrls'] = isset($expire['comment_custom_urls']) ?
                $expire['comment_custom_urls'] : '';
            $data['expireNavmenuFrontPage'] = isset($expire['navmenu_front_page']) ?
                $expire['navmenu_front_page'] : '';
            $data['expireNavmenuLink'] = isset($expire['navmenu_menu_link']) ?
                $expire['navmenu_menu_link'] : '';
            $data['expireNavmenuCustomUrl'] = isset($expire['navmenu_custom_url']) ?
                $expire['navmenu_custom_url'] : '';
            $data['expireNavmenuCustomUrls'] = isset($expire['navmenu_custom_urls']) ?
                $expire['navmenu_custom_urls'] : '';
            $data['expireWpThemeFrontPage'] = isset($expire['wp_theme_front_page']) ?
                $expire['wp_theme_front_page'] : '';
            $data['expireWpThemePurgeAll'] = isset($expire['wp_theme_purge_all']) ?
                $expire['wp_theme_purge_all'] : '';
            $data['expireWpThemeCustomUrl'] = isset($expire['wp_theme_custom_url']) ?
                $expire['wp_theme_custom_url'] : '';
            $data['expireWpThemeCustomUrls'] = isset($expire['wp_theme_custom_urls']) ?
                $expire['wp_theme_custom_urls'] : '';
        }

        $data['expirePostCustomUrlClass'] = 'hide_custom_url';
        $data['expirePostChecked'] = '';

        if ($data['expirePostCustomUrl'] == 'custom_url') {
            $data['expirePostChecked'] = 'checked="checked"';
            $data['expirePostCustomUrlClass'] = 'show_custom_url';
        }

        $data['expireCommentCustomUrlClass'] = 'hide_custom_url';
        $data['expireCommentChecked'] = '';
        if ($data['expireCommentCustomUrl'] == 'custom_url') {
            $data['expireCommentChecked'] = 'checked="checked"';
            $data['expireCommentCustomUrlClass'] = 'show_custom_url';
        }

        $data['expireNavmenuCustomUrlClass'] = 'hide_custom_url';
        $data['expireNavmenuChecked'] = '';
        if ($data['expireNavmenuCustomUrl'] == 'custom_url') {
            $data['expireNavmenuChecked'] = 'checked="checked"';
            $data['expireNavmenuCustomUrlClass'] = 'show_custom_url';
        }

        $data['expireWpThemeCustomUrlClass'] = 'hide_custom_url';
        $data['expireWpThemeChecked'] = '';
        if ($data['expireWpThemeCustomUrl'] == 'custom_url') {
            $data['expireWpThemeChecked'] = 'checked="checked"';
            $data['expireWpThemeCustomUrlClass'] = 'show_custom_url';
        }

        $data['nounceField'] = $this->getNonceField('purgeAllCache');
        $data['formAction'] = str_replace('%7E', '~', $_SERVER['REQUEST_URI']);

        return $data;
    }

    /**
     * Gather template data for purge all view.
     *
     * @return array
     */
    public function getPurgeAllTemplateData()
    {
        $data = [];

        $message = '';

        $data['message'] = $message;
        $data['formAction'] = str_replace('%7E', '~', $_SERVER['REQUEST_URI']);
        $data['nounceField'] = $this->getNonceField('purgeAllCache');

        return $data;
    }

    /**
     * Gather template data for terminal view.
     *
     * @return array
     */
    public function getTerminalTemplateData()
    {
        // Variable to set the error messages.
        $data = [];

        // Get the options value.
        $data['varnishVersion'] = esc_html($this->Helper->getOption('varnish_version', ''));
        $data['varnishControlTerminal'] = esc_html($this->Helper->getOption('varnish_control_terminal', ''));
        $data['varnishControlKey'] = esc_html($this->Helper->getOption('varnish_control_key', ''));
        $data['varnishSocketTimeout'] = (int) $this->Helper->getOption(
            'varnish_socket_timeout',
            $this->Terminal->timeout
        );

        $data['varnishVersionOptions'] = [
            [
                'text' => '4.x / 5.x',
                'value' => 4,
                'selected' => $data['varnishVersion'] == 4 ? 'selected' : ''
            ],
            [
                'text' => '3.x',
                'value' => 3,
                'selected' => $data['varnishVersion'] == 3 ? 'selected' : ''
            ]
        ];


        $data['terminalStatuses'] = $this->Terminal->getStatuses();
        $data['okImage'] = $GLOBALS['purgeVarnishUrl'] . '/dist/image/ok.png';
        $data['errorImage'] = $GLOBALS['purgeVarnishUrl'] . '/dist/image/error.png';
        $data['nounceField'] = $this->getNonceField('pvTsetting');

        $data['formAction'] = str_replace('%7E', '~', $_SERVER['REQUEST_URI']);

        return $data;
    }

    /**
     * Get template data from file.
     *
     * @param string $filename
     * @return string
     */
    public function getTemplate($filename)
    {
        $filePath = $GLOBALS['purgeVarnishPath'] . 'source/templates/' . $filename;
        if (!file_exists($filePath)) {
            trigger_error($filePath . ' not found!', E_USER_ERROR);
            return false;
        }
        return file_get_contents($filePath);
    }

    /**
     * Get nounce field in string format.
     *
     * @param $key
     * @return string
     */
    private function getNonceField($key)
    {
        ob_start();
        wp_nonce_field($key);
        $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}
