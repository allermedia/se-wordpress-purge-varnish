<?php

namespace PurgeVarnish;

/**
 * Debug stuff.
 */
class Debug
{

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->basedir = wp_upload_dir()['basedir'];
    }

    /**
     * Do logging when in debug mode.
     *
     * @param string $logtext
     * @return void
     */
    public function log($logtext)
    {

        if ($GLOBALS['purgeVarnishDebug'] == true) {
            $filename = $this->basedir . '/purge_varnish_log.txt';
            try {
                $handle = fopen($filename, 'a');
                fwrite($handle, $logtext . "\n");
                fclose($handle);
            } catch (Exception $e) {
                error_log("Cannot open file $filename " . $e->getMessage() . "\n", 0);
            }
        }
    }
}
