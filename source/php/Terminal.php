<?php

namespace PurgeVarnish;

/**
 * Terminal call handler class.
 */
class Terminal
{
    private $defaultTerminals = '127.0.0.1:6082';
    public $timeout = 100;

    /**
     * Constructor.
     *
     * @param Debug $Debug
     * @param Helper $Helper
     */
    public function __construct(Debug $Debug, Helper $Helper)
    {
        $this->Debug = $Debug;
        $this->Helper = $Helper;

        $this->varnishVersion = floatval($this->Helper->getOption('varnish_version', 2.1));
        $this->controlKey = $this->Helper->getOption('varnish_control_key', '');
        $this->timeout = $this->Helper->getOption('varnish_socket_timeout', $this->timeout);
        $this->terminals = explode(' ', $this->Helper->getOption('varnish_control_terminal', $this->defaultTerminals));
    }

    /**
     * Getter for terminals.
     *
     * @return array
     */
    public function getTerminals()
    {
        $this->terminals = explode(' ', $this->Helper->getOption('varnish_control_terminal', $this->defaultTerminals));
        return $this->terminals;
    }

    /**
     * Getter for controlKey.
     *
     * @return string
     */
    public function getControlKey()
    {
        $this->controlKey = $this->Helper->getOption('varnish_control_key', '');
        return $this->controlKey;
    }
    /**
     * Utilizes sockets to talk to varnish terminal and send the command to Varnish.
     *
     * @param string|array $command
     * @return array
     */
    public function run($commands)
    {
        if (!is_array($commands)) {
            $commands = [$commands];
        }

        if (!$this->isExtensionLoaded()) {
            return false;
        }
        // Convert single commands to an array to handle everything in the same way.
        $return = [];
        foreach ($this->getTerminals() as $terminal) {
            $client = $this->getClient($terminal);
            list($server, $port) = explode(':', $terminal);

            if (@!socket_connect($client, $server, $port)) {
                $this->logConnectionFailed($client, $server, $port);
                $return[$terminal] = false;
                continue;
            }

            $this->skipBanner($client);
            foreach ($commands as $command) {
                $this->Debug->log($command);

                if ($status = $this->executeCommand($client, $command)) {
                    $return[$terminal][$command] = $status;
                }

                if (is_array($status)) {
                    $this->Debug->log(json_encode($status));
                }
            }
            socket_close($client);
        }
        return $return;
    }

    /**
     * Get statuses of all terminals.
     *
     * @return array
     */
    public function getStatuses()
    {
        $terminalResponse = $this->run('status');
        $terminalStatuses = [];
        foreach ($this->getTerminals() as $terminal) {
            $responseStatus = isset($terminalResponse[$terminal]['status'])
                ? $terminalResponse[$terminal]['status'] : '';
            $statusCode = isset($responseStatus['code']) ? $responseStatus['code'] : '';
            $statusMessage = isset($responseStatus['msg']) ? $responseStatus['msg'] : '';
            if ($statusCode != 200) {
                $errorMessage = 'Error code: ' . $statusCode . ', ' . $statusMessage;
            } else {
                $errorMessage = '';
            }

            $terminalStatuses[] = [
                'terminal' => $terminal,
                'responseStatus' => $responseStatus,
                'statusCode' => $statusCode,
                'statusMessage' => $statusMessage,
                'errorMessage' => $errorMessage
            ];
        }
        return $terminalStatuses;
    }

    /**
     * Helper function to check if extension.
     *
     * @return boolean
     */
    private function isExtensionLoaded()
    {
        // Prevent fatal errors if requirements don't have meet.
        if (!extension_loaded('sockets')) {
            $logdata = 'You need to enable/install sockets module.';
            $this->Debug->log($logdata);
            error_log($log, 0);
            return false;
        }
        return true;
    }

    /**
     * Log failing onnection.
     *
     * @param resource $client
     * @param string   $server
     * @param string   $port
     * @return void
     */
    private function logConnectionFailed($client, $server, $port)
    {
        $logdata = 'Unable to connect to server socket '
            . $server . ':' . $port . ' -: ' . socket_strerror(socket_last_error($client));
        $this->Debug->log($logdata);
        error_log($logdata, 0);
    }

    /**
     * Gets client resource.
     *
     * @return resource
     */
    private function getClient()
    {
        // Convert varnish_socket_timeout timeout into milliseconds.
        $seconds = (int) ($this->timeout / 1000);
        $microseconds = (int) ($this->timeout % 1000 * 1000);

        $client = socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
        socket_set_option($client, SOL_SOCKET, SO_SNDTIMEO, ['sec' => $seconds, 'usec' => $microseconds]);
        socket_set_option($client, SOL_SOCKET, SO_RCVTIMEO, ['sec' => $seconds, 'usec' => $microseconds]);

        return $client;
    }

     /**
     * Send command to varnish.
     *
     * @param resource $client
     * @param string   $command
     * @return void
     */
    private function executeCommand($client, $command)
    {
        // Send command and get response.
        $result = socket_write($client, "$command\n");
        $status = $this->readSocket($client);
        if ($status['code'] != 200) {
            error_log('Recieved status code <i>' . $status['code'] . '</i> running '
                . $command . '. Full response text: <i>' . $status['msg'] . '</i>', 0);
            return $status;
        } else {
            // Successful connection.
            return $status;
        }
    }

    /**
     * Helper function to skip banner sent from Vanrish terminal.
     *
     * @param resource $client
     * @return void
     */
    private function skipBanner($client)
    {
        // If there is a CLI banner message (varnish >= 2.1.x), try to read it and
        // move on.
        if ($this->varnishVersion > 2.0) {
            $status = $this->readSocket($client);
            // Do we need to authenticate?
            // Require authentication.
            if ($status['code'] == 107) {
                $challenge = substr($status['msg'], 0, 32);
                $pack = $challenge . "\x0A" . $this->getControlKey() . $challenge . "\x0A";
                $key = hash('sha256', $pack);
                socket_write($client, "auth $key\n");
                $status = $this->readSocket($client);

                if ($status['code'] != 200) {
                    $logdata = 'Authentication to server failed!';
                    $this->Debug->log($logdata);
                    error_log($logdata, 0);
                }
            }
        }
    }

    /**
     * Socket read function.
     *
     * @param resource $client
     * @param integer  $retry
     * @return array
     */
    private function readSocket($client, $retry = 2)
    {
        // Status and length info is always 13 characters.
        $header = socket_read($client, 13, PHP_BINARY_READ);

        if ($header == false) {
            $error = socket_last_error();
            // 35 = socket-unavailable, so it might be blocked from our write.
            // This is an acceptable place to retry.
            if ($error == 35 && $retry > 0) {
                return $this->readSocket($client, $retry - 1);
            } else {
                error_log('Socket error: ' . socket_strerror($error), 0);
                return [
                  'code' => $error,
                  'msg' => socket_strerror($error),
                ];
            }
        }
        $messageLength = (int) substr($header, 4, 6) + 1;

        $status = [
          'code' => substr($header, 0, 3),
          'msg' => socket_read($client, $messageLength, PHP_BINARY_READ),
        ];

        return $status;
    }
}
