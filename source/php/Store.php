<?php

namespace PurgeVarnish;

/**
 * Handle the storing and validation of data.
 */
class Store
{

    /**
     * Constructor.
     *
     * @param Template $Template
     * @param Helper   $Helper
     */
    public function __construct(Helper $Helper, Template $Template)
    {
        $this->Helper = $Helper;
        $this->Template = $Template;
    }

    /**
     * Checks if save in progress and stores expire stuff.
     *
     * @param string $defaultActions
     * @param string $sanitizeTriggers
     * @return void
     */
    public function storeExpire($defaultActions, $defaultTriggers)
    {
        // Save the options value.
        if (isset($_POST['save_configurations']) && $_POST['save_configurations']) {
            if ($this->nonce('pvEsetting') == true) {
                $postActions = (array)$_POST['purge_varnish_action'];
                $sanitizeActions = $this->sanitizeActions($postActions, $defaultActions);
                $this->Helper->updateOption('purge_varnish_action', serialize($sanitizeActions));

                $postExpire = (array) $_POST['purge_varnish_expire'];
                $sanitizeTriggers = $this->sanitizeTriggers($postExpire, $defaultTriggers);
                $this->Helper->updateOption('purge_varnish_expire', serialize($sanitizeTriggers));
            }
        }
    }

    /**
     * Checks if save in progress and stores terminal stuff.
     *
     * @return void
     */
    public function storeTerminal()
    {
        // Save the options value.
        if (isset($_POST['savecode'])) {
            if ($this->nonce('pvTsetting') == true) {
                $data['validateMessage'] = $this->validate($_POST);
                $this->Helper->updateOption('varnish_version', sanitize_text_field($_POST['varnish_version']));
                $this->Helper->updateOption('varnish_control_terminal', sanitize_text_field($_POST['varnish_control_terminal']));
                $this->Helper->updateOption('varnish_control_key', sanitize_text_field($_POST['varnish_control_key']));
                $this->Helper->updateOption('varnish_socket_timeout', (int) $_POST['varnish_socket_timeout']);
            }
        }
    }

    /**
     * Checks if save in progress and stores multisite stuff.
     *
     * @return void
     */
    public function storeMultisite()
    {
        // Save the options value.
        if (isset($_POST['save_configurations']) && $_POST['save_configurations']) {
            if ($this->nonce('purgeMultisite') == true) {
                if (isset($_POST['purge_varnish_multisite_action'])) {
                    $postActions = (array)$_POST['purge_varnish_multisite_action'];
                    $this->Helper->updateOption('purge_varnish_multisite_action', $postActions);
                } else {
                    $this->Helper->updateOption('purge_varnish_multisite_action', []);
                }

                if (isset($_POST['purge_varnish_multisite_expire'])) {
                    $postExpire = (array) $_POST['purge_varnish_multisite_expire'];
                    $this->Helper->updateOption('purge_varnish_multisite_expire', $postExpire);
                } else {
                    $this->Helper->updateOption('purge_varnish_multisite_expire', []);
                }

                // Always use update_site_option/delete_site_option to avoid fetching/storing from wrong place.
                if (isset($_POST['purge_varnish_sitewide_options'])) {
                    update_site_option('purge_varnish_sitewide_options', true);
                    $this->Helper->switchOption(true);
                } else {
                    delete_site_option('purge_varnish_sitewide_options');
                    $this->Helper->switchOption(false);
                }
            }
        }
    }

    /**
     * Callback to validate post nonce.
     *
     * @param string $vpNonce
     * @return string|boolean
     */
    public function nonce($vpNonce)
    {
        $wpNonce = $_REQUEST['_wpnonce'];

        if (!wp_verify_nonce($wpNonce, $vpNonce)) {
            return '<ul><li style="color:#8B0000;">Sorry! Invalid nonce.</li></ul>';
        }

        return true;
    }

    /**
     * Sanitize actions values.
     *
     * @param WP_Post $post
     * @return array
     */
    private function sanitizeActions($post, $defaultActions)
    {
        $additionalActions = [
          'comment_status_changed' => 'transition_comment_status',
        ];
        $actions = $defaultActions + $additionalActions;
        $sanitize = [];
        foreach ($post as $key => $value) {
            if (array_key_exists($key, $actions) && ($actions[$key] == $value)) {
                $sanitize[$key] = $value;
            }
        }
        return $sanitize;
    }

    /**
     * Sanitize triggers values.
     *
     * @param WP_Post $post
     * @return array
     */
    private function sanitizeTriggers($post, $defaultTriggers)
    {
        $additionalTriggers = [
          'comment_post_item' => 'post_item',
          'navmenu_menu_link' => 'post_item',
          'wp_theme_purge_all' => 'purge_all',
          'post_custom_urls' => '?',
          'comment_custom_urls' => '?',
          'navmenu_custom_urls' => '?',
          'wp_theme_custom_urls' => '?',
        ];
        $triggers = $defaultTriggers + $additionalTriggers;

        $sanitize = [];
        foreach ($post as $key => $value) {
            if (array_key_exists($key, $triggers) && ($triggers[$key] == $value)) {
                $sanitize[$key] = $value;
            } elseif (array_key_exists($key, $triggers) && ($triggers[$key] == '?')) {
                $sanitize[$key] = $value;
            }
        }

        return $sanitize;
    }

    /**
     * Configuration save validator.
     *
     * @param array $post
     * @return string
     */
    private function validate($post)
    {
        $message = [];
        $varnishControlTerminal = trim($post['varnish_control_terminal']);
        $varnishControlTerminal = !empty($varnishControlTerminal) ? $varnishControlTerminal : '';
        $varnishControlKey = trim($post['varnish_control_key']);
        $varnishSocketTimeout = trim($post['varnish_socket_timeout']);

        if (empty($varnishControlTerminal) === true) {
            $message['error']['terminal_empty'] = esc_html('Varnish Control Terminal can\'t be empty.');
        }

        if (empty($varnishControlKey) === true) {
            $message['error']['key_empty'] = esc_html('Varnish Control Key can\'t be empty.');
        }

        if (empty($post['varnish_socket_timeout']) === true) {
            $message['error']['terminal_timeout'] = esc_html('Varnish connection timeout can\'t be empty.');
        }

        if (!empty($varnishControlTerminal)) {
            $terminals = explode(' ', $varnishControlTerminal);
            foreach ($terminals as $terminal) {
                list($server, $port) = explode(':', $terminal);

                if (isset($server) && (filter_var($server, FILTER_VALIDATE_IP) === false)) {
                    $message['error']['terminal_' . $server] = $server . ' ' . esc_html('is not a valid IP address');
                }
                if (empty($port) || (int) $port <= 0) {
                    $message['error']['terminal_' . $port] =
                        esc_html('You need to enter the valid Port Number with IP address');
                }
            }
        }

        if (!is_numeric($varnishSocketTimeout) || $varnishSocketTimeout <= 0) {
            $message['error']['varnish_socket_timeout'] =
                esc_html('Varnish connection timeout must be a positive number.');
        }

        $returnMessage = '';
        if (isset($message['error']) && count($message['error'])) {
            $returnMessage .= '<ul>';
            foreach ($message['error'] as $value) {
                $returnMessage .= '<li style="color:#FF0000;">' . $value . '</li>';
            }
            $returnMessage .= '</ul>';
        }

        return $returnMessage;
    }
}
