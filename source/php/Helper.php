<?php

namespace PurgeVarnish;

/**
 * Helper function class.
 */
class Helper
{

    public $sitewide;
    /**
     * Constructor.
     *
     */
    public function __construct()
    {
        $this->sitewide = get_site_option('purge_varnish_sitewide_options', false);
    }

    /**
     * Check if post is post object.
     *
     * @param WP_Post $post
     * @return boolean
     */
    public function isPostObject($post)
    {
        if ((is_object($post) && isset($post->post_type))) {
            return true;
        }
        return false;
    }

    /**
     * Check if post is post is nav menu item.
     *
     * @param WP_Post $post
     * @return boolean
     */
    public function postIsNavMenuItem($post)
    {
        if (is_object($post->post_type) && $post->post_type == 'nav_menu_item') {
            return true;
        }
        return false;
    }

    /**
     * Check if post is publish status.
     *
     * @param WP_Post $post
     * @return boolean
     */
    public function isPublishPostObject($post)
    {
        if (is_object($post) || $post->post_status == 'publish') {
            return true;
        }
        return false;
    }

    /**
     * Check if post is attachment.
     *
     * @param WP_Post $post
     * @return boolean
     */
    public function postIsAttachment($post)
    {
        if (is_object($post->post_type) && $post->post_type == 'attachment') {
            return true;
        }
        return false;
    }

    /**
     * Helper function to parse the host from the global $base_url.
     *
     * @param string $url
     */
    public function parseUrl($url)
    {
        $parts = parse_url($url);
        return $parts;
    }

    /**
     * Get suffix based on number.
     *
     * @param integer $number
     * @return string
     */
    public function numberSuffix($number)
    {
        if (!in_array(($number % 100), [11, 12, 13])) {
            switch ($number % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:
                    return $number . 'st';
                case 2:
                    return $number . 'nd';
                case 3:
                    return $number . 'rd';
            }
        }
        return $number . 'th';
    }

    /**
     * Get options based on where its should be set.
     *
     * @param string $option
     * @param mixed  $default
     * @return string
     */
    public function getOption($option, $default = '')
    {
        if ($this->sitewide) {
            return get_site_option($option, $default);
        } else {
            return get_option($option, $default);
        }
    }

    /**
     * Update options based on where its should be set.
     *
     * @param string $option
     * @param mixed  $value
     * @return void
     */
    public function updateOption($option, $value)
    {
        if ($this->sitewide) {
            update_site_option($option, $value);
        } else {
            update_option($option, $value, false);
        }
    }

    /**
     * Delete options on all possible option tables.
     *
     * @param sting $option
     * @return void
     */
    public function deleteOption($option)
    {
        delete_option($option);
        delete_site_option($option);
    }

    /**
     * Switch the options table.
     *
     * @param boolean $sitewide
     * @return void
     */
    public function switchOption($sitewide)
    {
        $options = [
            'purge_varnish_action',
            'purge_varnish_expire',
            'purge_varnish_multisite_action',
            'purge_varnish_multisite_expire',
            'varnish_version',
            'varnish_control_terminal',
            'varnish_control_key',
            'varnish_socket_timeout'
        ];

        if ($sitewide) {
            foreach ($options as $option) {
                $value = get_option($option, false);
                if ($value) {
                    delete_option($option);
                    update_site_option($option, $value);
                }
            }
            $this->sitewide = $sitewide;
        } else {
            foreach ($options as $option) {
                $value = get_site_option($option, false);
                if ($value) {
                    delete_site_option($option);
                    update_option($option, $value, false);
                }
            }
            $this->sitewide = $sitewide;
        }
    }
}
