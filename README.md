# Purge Varnish #
Purge Varnish Cache provides integration between your WordPress site and multiple Varnish Cache servers.

## Install ##
Enable the plugin and go to Terminal settings to set up the varnish endpoint.

## Developers notes ##

### Composer ###

Run ```composer update``` to get latest dependencies. The /vendor/-folder is gitignored.

If you run ```composer install``` you'll get the latest *locked* dependencies.

### Gulp ###

We also use Gulp, so you need to install and run it to build the css and javascript.

Install Gulp + dependencies:
```
npm install
```

Generate css and javascript for dist by running Gulp:
```
gulp
```

### PHP-DI ###
We use [PHP-DI](http://php-di.org/) to handle our dependency in classes. It is fairly basic, but good to know.

*In the main plugin file:*

```
$plugin = $container->get('Plugin');
```

*In source/php/Plugin.php*

```
public function __construct(Helper $Helper)
{
  $this->Helper = $Helper;
}
```
So when we create an instance of the Plugin-class, php-di automaticly fixes the dependency injection of Helper-class that is needed in the construct-method.

### Version numbers ###

After commiting to master you should increase the version number.   
There is a gulp task to make this easier.   
Type `gulp bump` to increment above files automatically.

*Note:*

The task will increment version in package.json and use that information to try and find a Wordpress plugin annotation in all php files in the root.

*Example:*

```
#Increase with a patch (default)
$ gulp bump
[13:40:47] Starting 'bump'...
[13:40:47] Finished 'bump' after 11 ms
[13:40:47] Bumped 1.0.0 to 1.0.1

#Increase with a minor
$ gulp bump --release minor
[13:40:47] Starting 'bump'...
[13:40:47] Finished 'bump' after 11 ms
[13:40:47] Bumped 1.0.1 to 1.1.0

#Increase with a major
$ gulp bump --release major
[13:40:47] Starting 'bump'...
[13:40:47] Finished 'bump' after 11 ms
[13:40:47] Bumped 1.1.0 to 2.0.0

#Increase with a pre-release
$ gulp bump --release prerelease
[13:40:47] Starting 'bump'...
[13:40:47] Finished 'bump' after 11 ms
[13:40:47] Bumped 2.0.0 to 2.0.0-0
```

## Code standard ##
We are using PSR-2 for PHP and air-bnb for javascript.  
To test this, run `npm test`

## Deployment notes ##

When deploying, make sure it also runs ```composer install``` and ```gulp build``` which requires ```npm install``` . This can be done in Jenkins via additional build step called execute shell. We have a nice deploy script to run called deploy-build.sh.

*Example:*
```
bash -ex bin/deploy-build.sh
```