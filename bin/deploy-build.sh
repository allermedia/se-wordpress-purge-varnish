#!/bin/bash

# Deploy script for fetching dependencies and removing unwanted files.
rm -rf vendor
rm -rf node_modules

composer install --no-dev
npm install
gulp build

rm -rf node_modules
rm -rf .git

rm composer.json
rm composer.lock
rm gulpfile.js
rm package.json
rm package-lock.json
rm README.md
rm .editorconfig
rm .eslintrc
rm .gitattributes
rm .gitignore
rm phpcs.xml.dist

rm -rf bin